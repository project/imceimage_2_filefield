<?php
// $Id$

// if true, will re-load node and compare updated fields, writing to watchdog any diff's found
define('IMCEIMAGE_2_FILEFIELD_DIFF', FALSE);

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// hooks

/**
 * Implementation of hook_init().
 */
function imceimage_2_filefield_init() {
  module_load_include('module', 'content');
}

/**
 * implementation of hook_help()
 */
function imceimage_2_filefield_help($path = NULL, $arg = NULL) {
  $output = '';
  switch ($path) {
    case "admin/help#imceimage_2_filefield":
      $output = '<p>'.  t("Migrates imceimage cck fields to filefield cck fields.") .'</p>';
      break;
  }
  return $output;
}

/**
 * implementation of hook_perm()
 */
function imceimage_2_filefield_perm() {
  return array(
    'administer imceimage_2_filefield', 
  );
}

/**
 * Implementation of hook_menu().
 */
function imceimage_2_filefield_menu() {

  $items = array();
  
  $items['admin/settings/imceimage_2_filefield'] = array(
    'title' => 'Migrate imceimage CCK Fields to filefield',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('imceimage_2_filefield'),
    'access arguments' => array('administer imceimage_2_filefield'),
    '#type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// batch

/**
 * the batch initiate form
 */
function imceimage_2_filefield() {
  $form = array();
  
  if (!empty($_SESSION['my_batch_results'])) {
    foreach ($_SESSION['my_batch_results'] as $result) {
      drupal_set_message($result);
    }
    unset($_SESSION['my_batch_results']);
  }

  $fields = array();
  foreach (content_fields() as $field) {
    if ($field['type'] == 'imceimage') { 
      $fields[] = $field; 
    }
  }
  $disabled = count($fields) < 1;
  
  if ($disabled) {
    form_set_error('', t('No imceimage cck fields found.'));
  }
  
  $form['description'] = array(
      '#value' => 
    		'<p>'. t('Migrates imceimage cck fields to filefield or imagefield cck fields. All referenced files will be copied to the drupal files directory, in the subdirectory specified below.') . '</p>' . 
        '<p>'. t('You should prevent any content editing while this process is running.') . '</p>' . 
        '<p>'. t('You should NOT trust this module will migrate your data perfectly. If it fails you must restore your site from a backup. Test on a copy! Make backups!') . '</p>' 
    );
  
  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['path']['dest'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Drupal files sub-directory to use as the destination for copied files.'),
    '#required' => TRUE,
    '#default_value' => 'filefield_images',
    '#disabled' => $disabled,
  );
  
  $form['path']['path_fieldname'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a sudirectory under the above path for each field.'),
    '#disabled' => $disabled,
    '#default_value' => 1,
  );
  
  $widgets = array();
  foreach (array('filefield', 'imagefield') as $module) {
    if ($items = module_invoke($module, 'widget_info')) {
      foreach ($items as $id => $widget) {
        $widgets[$id] = $widget['label'];
        $widget_default = $id;
      }
    }
  }
  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  foreach ($fields as $field) {
    $form['fields'][] = array(
      "import-{$field['field_name']}" => array(
        '#type' => 'checkbox',
        '#title' => $field['field_name'],
        '#default_value' => 1,
      ),
      "widget-{$field['field_name']}" => array(
        '#type' => 'select',
        '#options' => $widgets,
        '#default_value' => $widget_default,
      ),
    );
  }

  $form['performance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Performance'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['performance']['n_fields'] = array(
    '#type' => 'textfield',
    '#title' => t('Fields to delete per batch'),
    '#default_value' => 2,
    '#required' => TRUE,
    '#disabled' => $disabled,
  );

  $form['performance']['n_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Nodes to update per batch'),
    '#default_value' => 10,
    '#required' => TRUE,
    '#disabled' => $disabled,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('go'),
    '#disabled' => $disabled,
  );

  $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  return $form;
}

/**
 * form imceimage_2_filefield validation callback 
 */
function imceimage_2_filefield_validate($form = NULL, &$form_state = NULL) {
  if ($form_state['values']['n_fields'] < 1) {
    form_set_error('n_fields', t('Fields per batch must be greater than 0.'));
  }
  if ($form_state['values']['n_nodes'] < 1) {
    form_set_error('n_nodes', t('Nodes per batch must be greater than 0.'));
  }
    
  $path = $form_state['values']['dest'];
  $path = preg_replace(array('/^\/*/', '/\/*$/'), array('/', '/'), $path);
  $form_state['values']['dest_'] = $path;
  $path = file_directory_path() . $path;
  if (!file_check_directory($path, 1, 'path')) {
    form_set_error('dest', t('@path could not be written to.', array('@path' => $path)));
  }
}

/**
 * form imceimage_2_filefield submit callback 
 */
function imceimage_2_filefield_submit($form = NULL, &$form_state = NULL) {

  $n_fields = (int) $form_state['values']['n_fields'];
  $n_nodes = (int) $form_state['values']['n_nodes'];
  $path = $form_state['values']['dest_'];
  $path_fieldname = !empty($form_state['values']['path_fieldname']);
  
  $field_type = 'filefield';
    
  // cck field widget data
  $field_widgets = array();
  // cck field data - array keyed on field names, of array of content types containing the field
  $sources = array();
  foreach (content_fields() as $field) {
    if ($field['type'] == 'imceimage' && $form_state['values']["import-{$field['field_name']}"]) {
      $field_widgets[$field['field_name']] = $form_state['values']["widget-{$field['field_name']}"];
      foreach (content_types() as $type) {
        if ($f = content_fields($field['field_name'], $type['type'])) {
          $sources[$f['field_name']][] = $f['type_name'];
        }
      }
    }
  }
  
  if (count($sources)) {
    $operations = array(
      array('imceimage_2_filefield_batch_start', array()),
    );
    foreach ($sources as $field_name => $type_names) {
      $operations[] = array('imceimage_2_filefield_batch_save', array($field_name, $type_names));
      $operations[] = array('imceimage_2_filefield_batch_delete', array($n_fields, $field_name, $type_names));
      $operations[] = array('imceimage_2_filefield_batch_create', array($field_name, $field_type, $field_widgets[$field_name], $type_names, $path, $path_fieldname));
    }
    $operations[] = array('imceimage_2_filefield_batch_migrate', array($n_nodes, $sources, $path, $path_fieldname));
    $batch = array(
      'title' => t('Migrate imceimage CCK Fields to filefield'),
      'operations' => $operations,
      'finished' => 'imceimage_2_filefield_finish',
      'init_message' => t('Batch is starting.'),
      'progress_message' => t('Processing...'),
      'error_message' => t('Batch has encountered an error.'),
    );
    batch_set($batch);
  } 
  else {
    form_set_error('', 'Nothing to do.');
  }
}

/**
 * $context is not persistant across batch operations, so save such data in variables table
 * @param Array $context batchapi context
 */
function imceimage_2_filefield_batch_start(&$context) {
  $batch = array(
    'halt' => FALSE, // set to true to skip following operations
    'start' => time(),
  );
  variable_set("imceimage_2_filefield_batch_{$_GET['id']}", $batch);
}

/**
 * save imceimage source field data
 * @param String $field_name cck field name
 * @param Array $type_names content types as returned by content_fields()
 * @param Array $context batchapi context
 */
function imceimage_2_filefield_batch_save($field_name, $type_names, &$context) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  if (empty($batch) || $batch['halt']) {
    return;
  }
  
  // move data to imceimage_2_filefield table
  $field = content_fields($field_name);
  $query = 
     "INSERT INTO 
        {imceimage_2_filefield} (vid, nid, field_name, imceimage_path, imceimage_width, imceimage_height, imceimage_alt, delta) 
      SELECT 
        vid, nid, '{$field_name}', 
        {$field_name}_imceimage_path, {$field_name}_imceimage_width, {$field_name}_imceimage_height, {$field_name}_imceimage_alt
    ";
  $query .= $field['multiple'] ? ', delta' : ', 0';
  if ($field['db_storage']) {
    $query .= "
        FROM {content_type_{$type_names[0]}}
      ";
  } 
  else {
    $query .= "
        FROM {content_{$field_name}}
      ";
  }
  if (!db_query($query)) {
    $context['results'][] = imceimage_2_filefield_error('error saving @field_name data to imceimage_2_filefield table.', array('@field_name' => $field_name), TRUE);
    return;
  }
  
  $context['finished'] = 1;
}

/**
 * delete source field, delete all instances so we can create a new filefield with same name
 * @param Integer $n number of fields to process
 * @param String $field_name cck field name
 * @param Array $type_names content types as returned by content_fields()
 * @param Array $context batchapi context
 */
function imceimage_2_filefield_batch_delete($n, $field_name, $type_names, &$context) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  if (empty($batch) || $batch['halt']) {
    return;
  }
  
  if (!isset($context['sandbox']['i'])) {
    $context['sandbox']['i'] = 0;
  }
  $i = $context['sandbox']['i'];
  $N = count($type_names);
  
  module_load_include('inc', 'content', 'includes/content.crud');
  $types_deleted = array(); // to communicate to user
  $max = $i + $n; 
  if ($max > $N) {
    $max = $N;
  }
  while ($i < $max) {
    $type_name = $type_names[$i];
    
    // save metadata to use when creating filefield 
    $field = content_fields($field_name, $type_name);
    if (function_exists('_fieldgroup_field_get_group')) {
      $field['widget']['parent'] = _fieldgroup_field_get_group($type_name, $field_name);
    }
    $batch['fields'][$field_name][$type_name] = $field;
    
    // delete
    if (!content_field_instance_delete($field_name, $type_name)) {
      $context['results'][] = imceimage_2_filefield_error('error deleting @type_name @field_name.', array('@type_name' => $type_name, '@field_name' => $field_name), TRUE);
      return;
    }
    
    $i++;
    $types_deleted[] = $type_name;
  }
  variable_set("imceimage_2_filefield_batch_{$_GET['id']}", $batch);

  $context['finished'] = $i / $N;
  if ($context['sandbox']['i'] == $i || $context['finished'] > 1) {
    $context['finished'] = 1;
  }
  $context['sandbox']['i'] = $i;
  
  $context['message'] = t('Deleted @field_name from content types:  @types.', array('@types' => implode(', ', $types_deleted), '@field_name' => $field_name));
}

/**
 * creates the destination filefield
 * @param String $field_name cck field name
 * @param String $field_type cck field type
 * @param String $field_widget cck field widget
 * @param Array $type_names content types as returned by content_fields()
 * @param String $path path to copy files to
 * @param Boolean $path_fieldname use field name as part of path
 * @param Array $context batchapi context
 */
function imceimage_2_filefield_batch_create($field_name, $field_type, $field_widget, $type_names, $path, $path_fieldname, &$context) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  if (empty($batch) || $batch['halt']) {
    return;
  }
  
  module_load_include('inc', 'content', 'includes/content.crud');
  
  foreach ($type_names as $type_name) {
    
    // create a field array compatible with content_field_instance_create()
    $field = $batch['fields'][$field_name][$type_name];
    unset($batch['fields'][$field_name][$type_name]);

    $field['type'] = $field_type; 
    $field['module'] = $field_type;
    $field['widget']['type'] = $field_widget;
    $field['widget']['module'] = $field_type;
    
    // migrate field settings
    // file_path - no leading or trailing /
    $field['widget']['file_path'] = $path_fieldname ? substr($path, 1) . $field_name : substr($path, 1, strlen($path)-1);
    $field['widget']['file_extensions'] = $field['imceimage_file_types'];
    $field['widget']['custom_alt'] = 1;

    if (!$field = content_field_instance_create($field, FALSE)) {
      $context['results'][] = imceimage_2_filefield_error('error creating field @field_name.', array('@field_name' => $field_name), TRUE);
      return;
    }
    
    // fieldgroup is not part of field instance edit page, so 
    if (function_exists('fieldgroup_update_fields')) {
      fieldgroup_update_fields(array(
        'type_name' => $type_name, 
        'field_name' => $field_name,
        'group' => $field['widget']['parent'],
      ));
    }
  }

  variable_set("imceimage_2_filefield_batch_{$_GET['id']}", $batch);

  // @see content_field_instance_create()
  // Clear caches and rebuild menu.
  content_clear_type_cache(TRUE);
  menu_rebuild();

  $context['message'] = t('Created @field_name, added to content types: @types.', array('@types' => implode(', ', $type_names), '@field_name' => $field_name));
}

/**
 * migrates saved data to new filefield 
 * @param Integer $n number of nodes to process
 * @param Array $sources field name => content types
 * @param String $path path to copy files to
 * @param Boolean $path_fieldname use field name as part of path
 * @param Array $context batchapi context
 */
function imceimage_2_filefield_batch_migrate($n, $sources, $path, $path_fieldname, &$context) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  if (empty($batch) || $batch['halt']) {
    return;
  }
  
  // query {node} for instances of field
  $type_names = array();
  foreach ($sources as $field => $types) foreach ($types as $type) $type_names[$type] = $type;
  $query = "FROM {node} WHERE type IN ('" . implode("', '", $type_names) . "')";
  
  // total nodes to process
  if (!isset($context['sandbox']['N'])) {
    $N = db_result(db_query("SELECT COUNT(*) $query"));
    if (!$N) {
      return;
    }
    $context['sandbox']['N'] = $N;
  }
  $N = $context['sandbox']['N'];
  
  // resume batch
  if (!isset($context['sandbox']['i'])) {
    $context['sandbox']['i'] = 0;
  }
  $i = $context['sandbox']['i'];
  
  // each node
  $result = db_query_range("SELECT nid $query ORDER BY nid", array(), $i, $n);
  while ($r = db_fetch_object($result)) {
    // each revision
    $vids = imceimage_2_filefield_get_revisions($r->nid);
    foreach ($vids as $vid) {
      $node = node_load($r->nid, $vid);
      // each field
      foreach ($sources as $field_name => $type_names) {
        if (in_array($node->type, $type_names)) {
          // load the saved data for this field instance
          foreach (imceimage_2_filefield_get_items($r->nid, $vid, $field_name) as $item) {
          
            // @see http://drupal.org/node/330421
            // @see http://drupal.org/node/201594
    
            // the source imceimage
            $src = urldecode($item['imceimage_path']);
            if (empty($src)) { 
              continue;
            }
            // imceimage stores initial '/'
            $src = substr($src, 1);
            
            // the destination 
            $dest = file_directory_path() . $path;
            if ($path_fieldname) {
              $dest .= $field_name;
              if (!file_check_directory($dest, TRUE)) {
                $context['results'][] = imceimage_2_filefield_error('error checking directory @file_path.', array('@file_path' => $dest), TRUE);
                return;
              }
            }
            $dest .=  '/' . basename($src);
            
            if (!$file = imceimage_2_filefield_get_file($src, $dest, $node->uid)) {
              $context['results'][] = imceimage_2_filefield_error('error importing file @src to @dest.', array('@src' => $src, '@dest' => $dest), FALSE);
            } 
            else {
              // create filefield field
              $node->{$field_name}[$item['delta']] = array(
                'fid' => $file->fid,
                'title' => basename($file->filename),
                'filename' => $file->filename,
                'filepath' => $file->filepath,
                'filesize' => $file->filesize,
                'mimetype' => $file->filemime,
                'description' => basename($file->filename),
                'data' => array(
                  'alt' => $item['imceimage_alt'],
                  'title' => '',
                ),
                'list' => 1,
                'status' => 1,
              );
            }
          }
        }
      }

      // re-load node and compare updated fields, writing to watchdog any diff's found
      if (IMCEIMAGE_2_FILEFIELD_DIFF) {
        $node_presave = clone $node;
      }
      
      node_save($node);
      
      if (IMCEIMAGE_2_FILEFIELD_DIFF) {
        $node = node_load($r->nid, $vid, TRUE);
        foreach ($sources as $field_name => $type_names) {
          if (in_array($node->type, $type_names) && is_array($node_presave->{$field_name})) {
            if (!is_array($node->{$field_name})) {
              $context['results'][] = imceimage_2_filefield_error('error saving field @field_name to node @nid.', array('@field_name' => $field_name, '@nid' => $r->nid), FALSE);
            }
            if (FALSE !== $diff = imceimage_2_filefield_diff($node->{$field_name}, $node_presave->{$field_name})) {
              watchdog('imceimage_2_filefield', 'error saving field @field_name to node @nid: !diff.', array('@field_name' => $field_name, '@nid' => $r->nid, '@diff' => print_r($diff, TRUE)), WATCHDOG_NOTICE);
            }
          }
        }
      }
    }
    $i++;
  }
  
  $context['finished'] = $i / $N;
  if ($context['sandbox']['i'] == $i || $context['finished'] > 1) {
    $context['finished'] = 1;
  }
  $context['sandbox']['i'] = $i;

  $context['message'] = t('Migrating data, finished @i out of @N.', array('@i' => $i, '@N' => $N));
}

/**
 * batchapi finish callback
 */
function imceimage_2_filefield_finish($success, $results, $operations) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  variable_del("imceimage_2_filefield_batch_{$_GET['id']}");
  $halt = $batch['halt'];
  $time = time() - $batch['start'];

  $success = $success && !$halt;
  if ($success) {
    $message = t('Finished successfully.');
  }
  else {
    $message = $halt ? t('Migration incomplete.') : t('Finished with an error.');
  }
  drupal_set_message($message . ' (' . $time . 's)');

  // Providing data for the redirected page is done through $_SESSION.
  $_SESSION['my_batch_results'] = $results;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
// utils

/**
 * gets all revision id's for a node for which imceimage data needs to be migrated.
 * @param integer $nid
 * @param string $field_name
 */
function imceimage_2_filefield_get_revisions($nid) {
  $query = "SELECT DISTINCT vid FROM {imceimage_2_filefield} WHERE nid = %d";
  $param = array($nid);
  $result = db_query($query, $param);
  $vids = array();
  while ($r = db_fetch_object($result)) {
    $vids[] = $r->vid;
  }
  return $vids;
}

/**
 * gets all imceimage instances of a field for a specific revision, marks as migrated.
 * @param Integer $nid {node}.nid
 * @param Integer $vid {node}.vid
 * @param String $field_name imceimage cck field name
 */
function imceimage_2_filefield_get_items($nid, $vid, $field_name) {
  $query = "SELECT * FROM {imceimage_2_filefield} WHERE nid = %d AND vid = %d AND field_name = '%s' AND status = 0";
  $param = array($nid, $vid, $field_name);
  $result = db_query($query, $param);
  $items = array();
  while ($r = db_fetch_array($result)) {
    $r['status'] = 1;
    drupal_write_record('imceimage_2_filefield', $r, array('nid', 'vid', 'delta', 'field_name'));
    $items[] = $r;
  }
  return $items;
}

/**
 * migrates a imceimage file to a drupal file. Paths are relative to drupal install.
 * @see field_file_save_file() may want to use this instead
 * @param String $src source path
 * @param String $dest destination path
 * @param Integer $uid {user}.uid
 */
function imceimage_2_filefield_get_file($src, $dest, $uid) {
  // already copied?
  $result = db_query("SELECT f.* FROM {imceimage_2_filefield_files} i INNER JOIN {files} f ON i.fid = f.fid WHERE i.path = '%s'", array($src));
  if ($r = db_fetch_object($result)) {
    return $r;
  }
  // copy file
  $file_temp = file_get_contents($src);
  if (!$file_temp = file_save_data($file_temp, $dest, FILE_EXISTS_RENAME)) {
    return FALSE;
  }
  // save as drupal file, create fid
  $file = new stdClass();
  $file->filename = basename($file_temp);
  $file->filepath = $file_temp;
  $file->filemime = file_get_mimetype(basename($file_temp));
  $file->filesize = filesize($file_temp);
  $file->uid = $uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->timestamp = time();
  drupal_write_record('files', $file);
  if (!empty($file->fid)) {
    $r = array('fid' => $file->fid, 'path' => $src);
    drupal_write_record('imceimage_2_filefield_files', $r);
    return $file;
  }
  return FALSE;
}

/**
 * logs the error and returns the translated message, optionally sets the batch halt flag
 * @param String watchdog() $message
 * @param Array $var watchdog() $variables
 * @param Boolean $halt kill the batch
 */
function imceimage_2_filefield_error($msg, $var, $halt) {
  $batch = variable_get("imceimage_2_filefield_batch_{$_GET['id']}", FALSE);
  watchdog('imceimage_2_filefield', $msg, $var, WATCHDOG_ERROR);
  $batch['halt'] = $halt;
  variable_set("imceimage_2_filefield_batch_{$_GET['id']}", $batch);
  return t($msg, $var);
}

/**
 * compares node cck field arrays 
 * @see http://us.php.net/manual/en/function.array-diff-assoc.php#89635
 * @param Array $a 
 * @param Array $b
 */
function imceimage_2_filefield_diff($a, $b) {
  $diff = FALSE;
  // Left-to-right
  foreach ($a as $key => $value) {
    if (!array_key_exists($key, $b)) {
      $diff[0][$key] = $value;
    } 
    elseif (is_array($value)) {
      if (!is_array($b[$key])) {
        $diff[0][$key] = $value;
        $diff[1][$key] = $b[$key];
      } 
      else {
        $new = imceimage_2_filefield_diff($value, $b[$key]);
        if ($new !== FALSE) {
          if (isset($new[0])) $diff[0][$key] = $new[0];
          if (isset($new[1])) $diff[1][$key] = $new[1];
        };
      };
    } 
    elseif ($b[$key] !== $value) {
      $diff[0][$key] = $value;
      $diff[1][$key] = $b[$key];
    };
  };
  // Right-to-left
  foreach ($b as $key => $value) {
    if (!array_key_exists($key, $a)) {
      $diff[1][$key] = $value;
    };
    // No direct comparsion because matching keys were compared in the
    // left-to-right loop earlier, recursively.
  };
  return $diff;
}

