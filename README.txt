Migrates imceimage cck fields to filefield or imagefield cck fields. All referenced files will be copied to the drupal files directory, in a specified subdirectory.

You should prevent any content editing while this process is running.

You should NOT trust this module will migrate your data perfectly. If it fails you must restore your site from a backup. Test on a copy! Make backups!

Usage:

1. Install filefield and imceimage_2_filefield modules in the normal way.
2. Optionally install imagefield module.
3. Grant "administer imceimage_2_filefield" permissions - /admin/user/permissions
4. Configure and run - admin/settings/imceimage_2_filefield

Views

You may have to fix any references to the migrated fields

Themes

If you write an image tag in a theme, you will find that filefield paths do not contain an initial forward slash like imceimage. Use or override theme_imagefield_image()

<?php print theme('imagefield_image', $node->field_myfield[0]); ?>